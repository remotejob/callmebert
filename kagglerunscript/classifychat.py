import torch
from transformers import BertForSequenceClassification, BertTokenizer
from scipy.special import softmax
import csv

datapath = '/kaggle/input/callmebertdata/'

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def get_reply(model, msg):

    with torch.no_grad():
        encoded_dict = tokenizer.encode_plus(
            msg,
            # add_special_tokens=True,
            return_attention_mask=True,
            # pad_to_max_length=True,
            padding='longest',
            truncation=True,
            max_length=512,
            # max_length=64,           # Pad & truncate all sentences.
            # pad_to_max_length=True,
            # return_attention_mask=True,   # Construct attn. masks.
            return_tensors='pt',     # Return pytorch tensors.
        )
    input_ids = encoded_dict['input_ids'].to(device)
    attention_masks = encoded_dict['attention_mask'].to(device)
    res = model(input_ids, token_type_ids=None, attention_mask=attention_masks)
    top1_prob, top1label = torch.topk(res[0], 1)

    pred = top1label[0].item()
    # softmaxlabel = softmax(res[0].detach().cpu())
    softmaxlabel = softmax(res[0].detach().cpu().numpy())
    accuracy = softmaxlabel[0][pred]
    intent = intents.get(pred)

    return accuracy.item(), intent


intents = {}

chatphrases = []

with open(datapath+'intent.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    for row in csv_reader:
        # print(int(row['id']), row['intent'])
        intents[int(row['id'])] = row['intent']

with open(datapath+'classifydata.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    for row in csv_reader:
        # print(row['ask'])
        chatphrases.append(row['ask'])


tokenizer = BertTokenizer.from_pretrained(
    datapath+'best_model', do_lower_case=True)


model = BertForSequenceClassification.from_pretrained(
    datapath+'best_model',
    # num_labels=int(config['DEFAULT']['num_labels']),
    num_labels=11,
    output_attentions=False,  # Whether the model returns attentions weights.
    output_hidden_states=False,  # Whether the model returns all hidden-states.
)

model.to(device)


# csv.register_dialect('chatdialect', quoting=csv.QUOTE_MINIMAL)
with open('callmephrases.csv', mode='w') as f:
    fieldnames = ['ask', 'intent']
    writer = csv.DictWriter(f, fieldnames=fieldnames,quoting=csv.QUOTE_MINIMAL)

    for phrase in chatphrases:
        accuracy, intent = get_reply(model, phrase)
        if intent != 'other':
            # f.write(phrase+'\t' + intent + '\t'+str(accuracy)+'\n')
            # writer.writeheader()
            writer.writerow({'ask': phrase, 'intent': intent})

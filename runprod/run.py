import configparser
import csv
from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel
from scipy.special import softmax
import numpy as np
import torch
from transformers import BertForSequenceClassification, AdamW, BertConfig, BertTokenizer

config = configparser.ConfigParser()
config.read('config/config.ini')
print(config['DEFAULT']['num_labels'])
datapath = 'data/'

PORT = config['DEFAULT']['PORT']

intents = {}

with open(datapath+'intent.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=',')
    for row in csv_reader:
        print(int(row['id']), row['intent'])
        intents[int(row['id'])] = row['intent']


tokenizer = BertTokenizer.from_pretrained(
    datapath+'best_model', do_lower_case=True)

model = BertForSequenceClassification.from_pretrained(
    datapath+'best_model',
    num_labels=int(config['DEFAULT']['num_labels']),
    output_attentions=False,  # Whether the model returns attentions weights.
    output_hidden_states=False,  # Whether the model returns all hidden-states.
)


def get_reply(model, msg):

    with torch.no_grad():
        encoded_dict = tokenizer.encode_plus(
            msg,
            # add_special_tokens=True,
            return_attention_mask=True,
            pad_to_max_length=True,
            max_length=256,
            # max_length=64,           # Pad & truncate all sentences.
            # pad_to_max_length=True,
            # return_attention_mask=True,   # Construct attn. masks.
            return_tensors='pt',     # Return pytorch tensors.
        )
    input_ids = encoded_dict['input_ids'].cpu()
    attention_masks = encoded_dict['attention_mask'].cpu()
    res = model(input_ids, token_type_ids=None, attention_mask=attention_masks)
    top1_prob, top1label = torch.topk(res[0], 1)

    pred = top1label[0].item()
    # softmaxlabel = softmax(res[0].detach().cpu())
    softmaxlabel = softmax(res[0].detach().numpy())
    accuracy = softmaxlabel[0][pred]
    intent = intents.get(pred)

    return accuracy.item(), intent


class Itemask(BaseModel):
    ask: str


app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/translator/translate")
def predict(item: Itemask):
    ask = item.ask

    accuracy, intent = get_reply(model, ask)
    return {'Accuracy': accuracy, 'Answer': intent, 'Score': float(config['DEFAULT']['score'])}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(
        PORT), log_level="info", reload=False)
